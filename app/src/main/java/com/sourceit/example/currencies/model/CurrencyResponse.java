package com.sourceit.example.currencies.model;

import java.util.List;

/**
 * Created by wenceslaus
 * on 27.05.17.
 */

public class CurrencyResponse {

    public String date;
    public List<Organization> organizations;


    public static class Organization {
        public String title;
        public String address;
        public String phone;
    }
}
