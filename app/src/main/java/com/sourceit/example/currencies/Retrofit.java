package com.sourceit.example.currencies;

import com.sourceit.example.currencies.model.CurrencyResponse;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

/**
 * Created by wenceslaus on 27.05.17.
 */

public class Retrofit {

    private static final String ENDPOINT = "http://resources.finance.ua";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/ru/public/currency-cash.json")
        void getOrganizations(Callback<CurrencyResponse> callback);
    }

    public static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getOrganizations(Callback<CurrencyResponse> callback) {
        apiInterface.getOrganizations(callback);
    }
}
